<?php

return [
    'path'          => resource_path('themes'),

    'default'       => [
        'admin'     => 'admin-lte',
        'public'    => 'laravel',
    ],

    'breadcrumbs'   => [
        'default_view'  => 'breadcrumbs::bootstrap3'
    ]
];