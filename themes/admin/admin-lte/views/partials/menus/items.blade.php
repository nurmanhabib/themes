@foreach($items as $item)
  <li><a href="{{ $item->url() }}"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
@endforeach