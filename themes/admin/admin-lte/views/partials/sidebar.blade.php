<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="{{ auth()->user() ? auth()->user()->avatar ?: themes('admin')->asset('dist/img/user2-160x160.jpg') : themes('admin')->asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>{{ auth()->user() ? auth()->user()->getFullName() ?: 'Unknown' : 'Guest' }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <!-- sidebar menu: : style can be found in sidebar.less -->
    @include('admin-lte::partials.menus.primary')
    
  </section>
  <!-- /.sidebar -->
</aside>