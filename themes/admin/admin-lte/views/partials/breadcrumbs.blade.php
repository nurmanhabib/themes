@if ($breadcrumbs)
    <ol class="breadcrumb">
        @foreach ($breadcrumbs as $breadcrumb)
        	<?php
        	if ($faicon = $breadcrumb->icon) {
        		$icon = '<i class="fa fa-'.$faicon.'"></i> ';
        	} else {
        		$icon = '';
        	}
        	?>

            @if (!$breadcrumb->last)
                <li><a href="{{ $breadcrumb->url }}">{!! $icon !!}{{ $breadcrumb->title }}</a></li>
            @else
                <li class="active">{!! $icon !!}{{ $breadcrumb->title }}</li>
            @endif
        @endforeach
    </ol>
@endif