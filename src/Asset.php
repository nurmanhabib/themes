<?php

namespace Nurmanhabib\Themes;

class Asset
{
    protected $theme;

    public function __construct(ThemeInfo $theme)
    {
        $this->theme = $theme;
    }
}