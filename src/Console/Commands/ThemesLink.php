<?php

namespace Nurmanhabib\Themes\Console\Commands;

use Illuminate\Console\Command;
use Nurmanhabib\Themes\Themes;
use Nurmanhabib\Themes\Exceptions\AssetsNotFoundException;

class ThemesLink extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'themes:link {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create symbolic link theme assets';

    protected $themes;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Themes $themes)
    {
        parent::__construct();

        $this->themes = $themes;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin_themes   = $this->themes->getAdminThemes();
        $public_themes  = $this->themes->getPublicThemes();
        $force          = (bool) $this->option('force');

        $this->createLink($admin_themes, $force);
        $this->createLink($public_themes, $force);
    }

    protected function createLink($themes, $force = false)
    {
        foreach ($themes as $theme) {
            try {
                $linked = $this->themes->createAssetSymbolic($theme, $force);

                if ($linked)
                    $this->info('success: ' . $theme->getSlug() . ' berhasil dibuat symbolic link.');
                else
                    $this->comment('warning: ' . $theme->getSlug() . ' sudah ada symbolic link.');
            } catch (AssetsNotFoundException $e) {
                $this->comment('warning: ' . $theme->getSlug() . ' tidak memiliki direktori assets.');
            }
        }
    }
}