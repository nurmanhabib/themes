<?php

namespace Nurmanhabib\Themes\Components;

use DaveJamesMiller\Breadcrumbs\Facade as BreadcrumbsFacade;

class Breadcrumbs
{
	protected $breadcrumbs;

	public function __construct($app)
	{
		$this->breadcrumbs = $app['breadcrumbs'];
	}

	public function register($name, $callback)
	{
		$this->breadcrumbs->register($name, $callback);
	}
}