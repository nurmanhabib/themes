<?php

namespace Nurmanhabib\Themes;

use Illuminate\Support\ServiceProvider;

class ThemesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerBreadcrumbs();
        $this->publishConfig();
    }

    public function register()
    {
        $this->registerConfig();
        $this->registerViewFinder();
        $this->registerApp();
        $this->registerAppBreadcrumbs();
        $this->registerCommands();
    }

    protected function registerConfig()
    {        
        $this->mergeConfigFrom(__DIR__.'/../config/themes.php', 'themes');
    }

    protected function registerViewFinder()
    {
        $this->app->bind('view.finder', function ($app) {
            $paths = $app['config']['view.paths'];

            return new ThemeFileViewFinder($app['files'], $paths);
        });
    }

    protected function registerApp()
    {
        $this->app->singleton(Themes::class, function ($app) {
            $theme = new Themes(
                            $app['config'],
                            $app['events'],
                            $app['view'],
                            $app['files'],
                            $app['breadcrumbs']
                        );
            
            $paths  = [
                $theme->config('path'),
                __DIR__.'/../themes',
            ];

            foreach ($paths as $path) {
                $theme->loadFromPath($path);
            }

            return $theme;
        });

        $this->app->bind('themes', Themes::class);

        $theme          = app('themes');
        $default        = [
            'admin'     => $theme->config('default.admin'),
            'public'    => $theme->config('default.public'),
        ];

        if ($admin = $default['admin'])
            $theme->setActive($theme->find($admin, 'admin'));

        if ($public = $default['public'])
            $theme->setActive($theme->find($public, 'public'));
    }

    protected function registerCommands()
    {
        $this->commands([
            Console\Commands\ThemesLink::class
        ]);
    }

    protected function registerAppBreadcrumbs()
    {
        $this->app['breadcrumbs'] = $this->app->share(function ($app) {
            $this->loadViewsFrom(__DIR__.'/../breadcrumbs', 'breadcrumbs');
            $view = $app['config']['themes.breadcrumbs.default_view'];

            $manager = $this->app->make(\DaveJamesMiller\Breadcrumbs\Manager::class);
            $manager->setView($view);

            return $manager;
        });
    }

    protected function registerBreadcrumbs()
    {
        $breadcrumbs = $this->app['breadcrumbs'];
        
        // Load the app breadcrumbs if they're in routes/breadcrumbs.php (Laravel 5.3)
        if (file_exists($file = $this->app['path.base'].'/routes/breadcrumbs.php'))
        {
            require $file;
        }

        // Load the app breadcrumbs if they're in app/Http/breadcrumbs.php (Laravel 5.0-5.2)
        elseif (file_exists($file = $this->app['path'].'/Http/breadcrumbs.php'))
        {
            require $file;
        }
    }

    protected function publishConfig()
    {
        $this->publishes([
            __DIR__.'/../config/themes.php' => config_path('themes.php')
        ], 'config');
    }
}