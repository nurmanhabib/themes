<?php

if (!function_exists('themes')) {
	function themes($type = null) {
		$themes = app('themes');

		if ($type)
			$themes->setCurrentType($type);

		return $themes;
	}
}