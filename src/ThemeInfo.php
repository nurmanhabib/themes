<?php

namespace Nurmanhabib\Themes;

use SplFileInfo;

use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Contracts\Support\Arrayable;

class ThemeInfo implements Jsonable, Arrayable
{
    protected $slug;
    protected $name;
    protected $author;
    protected $description;
    protected $homepage;
    protected $version;
    protected $type;
    protected $path;

    protected $active = false;

    public function __construct($json = null, $path = null)
    {
        if ($json && $path) {
            $this->loadFromJson($json, $path);
        }
    }

    public function loadFromFile(SplFileInfo $filename)
    {
        $json   = file_get_contents($filename->getRealPath());
        $path   = $filename->getPath();

        return $this->loadFromJson($json, $path);
    }

    public function loadFromJson($json, $path)
    {
        $theme = json_decode($json, true);

        return $this->loadFromArray($theme, $path);
    }

    public function loadFromArray(array $theme, $path)
    {
        $this->path         = realpath($path);
        $this->slug         = array_get($theme, 'slug', $this->generateSlug());
        $this->name         = array_get($theme, 'name');
        $this->author       = array_get($theme, 'author');
        $this->description  = array_get($theme, 'description');
        $this->homepage     = array_get($theme, 'homepage');
        $this->version      = array_get($theme, 'version');
        $this->type         = array_get($theme, 'type', 'public');

        return $this;
    }

    public function setActive($active = true)
    {
        $this->active = (bool) $active;

        return $this;
    }

    public function isActive()
    {
        return $this->active === true;
    }

    public function generateSlug()
    {
        $parent_dir = basename($this->getPath());

        return str_slug($parent_dir);
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getHomepage()
    {
        return $this->homepage;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getLayoutView($view = 'app')
    {
        if (!$view) {
            $view = 'app';
        }

        return $this->getSlug() . '::layouts.' . $view;
    }

    public function getViewPath($path = null)
    {
        return $this->getPath('views/' . $path);
    }

    public function getAssetPath($path = null)
    {
        return $this->getPath('assets/' . $path);
    }

    public function getPath($path = null)
    {
        $path = !is_null($path) ? '/' . $path : '';

        return rtrim($this->path, '/\\') . $path;
    }

    public function toJson($options = 0)
    {
        return json_encode($this->toArray());
    }

    public function toArray()
    {
        return [
            'slug'          => $this->getSlug(),
            'name'          => $this->getName(),
            'author'        => $this->getAuthor(),
            'description'   => $this->getDescription(),
            'homepage'      => $this->getHomepage(),
            'version'       => $this->getVersion(),
            'type'          => $this->getType(),
            'active'        => $this->isActive(),
            'path'          => $this->getPath(),
        ];
    }

    public function __toString()
    {
        return $this->toJson();
    }
}