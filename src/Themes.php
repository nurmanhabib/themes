<?php

namespace Nurmanhabib\Themes;

use Closure;
use ReflectionClass;
use SplFileInfo;
use DaveJamesMiller\Breadcrumbs\Manager as Breadcrumbs;
use Illuminate\Http\Response;
use Illuminate\View\Factory;
use Illuminate\Config\Repository;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Cookie;

class Themes
{
    protected $config;
    protected $events;
    protected $view;
    protected $files;
    protected $breadcrumbs;

    protected $themes;
    protected $current_type;
    protected $active = [
        'admin'     => null,
        'public'    => null,
    ];

    protected $asset_dir = 'themes';

    public function __construct(Repository $config,
                                Dispatcher $events,
                                Factory $view,
                                Filesystem $files,
                                Breadcrumbs $breadcrumbs)
    {
        $this->config       = $config;
        $this->events       = $events;
        $this->view         = $view;
        $this->files        = $files;
        $this->breadcrumbs  = $breadcrumbs;

        $this->current_type = 'public';
        $this->themes       = [
            'admin'     => new Collection,
            'public'    => new Collection,
        ];
    }

    public function loadFromPath($path)
    {
        $path   = rtrim($path, '\//');
        $theme1 = glob($path . '/*/theme.json');
        $theme2 = glob($path . '/*/*/theme.json');
        $themes = array_merge($theme1, $theme2);

        foreach ($themes as $filename) {
            $this->loadFromFile(new SplFileInfo($filename));
        }
    }

    public function loadFromFile(SplFileInfo $filename)
    {
        $theme = new ThemeInfo;
        $theme->loadFromFile($filename);

        $this->registerTheme($theme);

        return $this;
    }

    public function registerTheme(ThemeInfo $theme)
    {
        $slug = $theme->getSlug();
        $type = $theme->getType();

        if (!$this->themes[$type]->has($slug)) {
            $this->themes[$type]->put($slug, $theme);

            $this->registerViewNamespace($theme);
        }
    }

    public function config($key = null, $default = null)
    {
        return $this->config->get('themes.' . $key, $default);
    }

    public function setTheme($slug)
    {
        $theme = $this->find($slug);

        return $this->setActive($theme);
    }

    public function setActive(ThemeInfo $theme)
    {
        $this->active[$theme->getType()] = $theme->setActive();

        $this->fire('changed', compact('theme'));

        $this->registerLayoutViewNamespace($theme);
        $this->registerBreadcrumbsView($theme);

        return $this;
    }

    public function getAdminActive()
    {
        return $this->getActive('admin');
    }

    public function getPublicActive()
    {
        return $this->getActive('public');
    }

    public function getActive($type = null)
    {
        if (is_null($type)) {
            $type = $this->getCurrentType();
        }
        
        return $this->active[$type];
    }

    public function getCurrentType()
    {
        return $this->current_type;
    }

    public function setCurrentType($type)
    {
        if (!array_key_exists($type, $this->themes)) {
            throw new Exceptions\TypeNotFoundException("Theme type [{$type}] tidak tersedia.");
        }

        $this->current_type = $type;

        return $this;
    }

    public function find($slug, $type = null)
    {
        if (is_null($type)) {
            $type = $this->getCurrentType();
        }

        $themes     = $this->lists($type);
        $selected   = $themes->first(function ($theme) use ($slug) {
            return $theme->getSlug() === $slug;
        });

        if ($selected) {
            return $selected;
        } else {
            throw new Exceptions\ThemeNotFoundException("Theme [{$type}.{$slug}] tidak terdaftar.");
        }
    }

    public function getAdminThemes()
    {
        return $this->lists('admin');
    }

    public function getPublicThemes()
    {
        return $this->lists('public');
    }

    public function getAllThemes()
    {
        return $this->lists();
    }

    public function lists($type = null)
    {
        if ($type) {
            if (!array_key_exists($type, $this->themes)) {
                throw new Exceptions\TypeNotFoundException("Theme type [{$type}] tidak tersedia.");
            }

            $themes = $this->themes[$type];
        } else {
            $themes = $this->themes;
        }

        return new Collection($themes);
    }

    public function registerViewNamespace(ThemeInfo $theme)
    {
        $this->view->addNamespace($theme->getSlug(), $this->localThemeView($theme));
        $this->view->addNamespace($theme->getSlug(), $theme->getViewPath());
    }

    public function registerLayoutViewNamespace(ThemeInfo $theme)
    {
        $this->view->addNamespace('layout-' . $theme->getType(), $this->localThemeView($theme, 'layouts'));
        $this->view->addNamespace('layout-' . $theme->getType(), $theme->getViewPath('layouts'));
    }

    public function registerViewLocation(ThemeInfo $theme)
    {
        $this->view->getFinder()->addThemeViewLocation($theme);
    }

    public function registerBreadcrumbsView(ThemeInfo $theme)
    {
        $view = $theme->getSlug().'::partials.breadcrumbs';

        if ($this->view->exists($view)) {
            $this->breadcrumbs->setView($view);
        }
    }

    public function createAssetSymbolic(ThemeInfo $theme, $force = false)
    {
        $files          = $this->files;
        $asset_path     = $theme->getAssetPath();
        $public_path    = public_path($this->asset_dir.'/'.$theme->getType().'/'.$theme->getSlug());

        return $this->createAssetThemesDir($theme, $force);
    }

    protected function createAssetThemesDir(ThemeInfo $theme, $force = false)
    {
        $themes_path    = public_path($this->asset_dir);
        $type_path      = public_path($this->asset_dir.'/'.$theme->getType());

        $this->createDirNotExists($themes_path);
        $this->createDirNotExists($type_path);

        $asset_path     = $theme->getAssetPath();
        $public_path    = public_path($this->asset_dir.'/'.$theme->getType().'/'.$theme->getSlug());

        if (!$this->files->isDirectory($asset_path)) {
            throw new Exceptions\AssetsNotFoundException('Directory assets pada theme ['.$theme->getSlug().'] tidak ada.');

            return false;
        }

        if ($this->files->exists($public_path)) {
            if ($force === false) {
                return false;
            } else {
                $this->files->delete($public_path);
            }
        }

        $this->files->link($asset_path, $public_path);

        return true;
    }

    protected function createDirNotExists($path)
    {
        if (!$this->files->isDirectory($path)) {
            $this->files->makeDirectory($path);
        }
    }

    public function fire($event, $payload = [])
    {
        $this->events->fire('themes.'.$event, $payload);
    }

    public function breadcrumbs($name = null, $data = null)
    {
        return $this->breadcrumbs->renderIfExists($name, $data);
    }

    public function getLayoutView($view = 'app')
    {
        $theme = $this->getActive();

        return $theme->getLayoutView($view);
    }

    public function page($view, $data = [], $mergeData = [])
    {
        return $this->view('pages.'.$view, $data, $mergeData);
    }

    public function view($view, $data = [], $mergeData = [])
    {
        $this->registerViewLocation($this->getActive());

        return $this->view->make($view, $data, $mergeData);
    }

    public function bower($path = '')
    {
        return $this->asset('bower_components/'.$path);
    }

    public function asset($path = '')
    {
        $theme  = $this->getActive();
        $prefix = $this->asset_dir;
        $type   = $theme->getType();
        $slug   = $theme->getSlug();

        $asset_path = compact('prefix', 'type', 'slug');
        $asset_path = implode('/', $asset_path);

        return asset(trim($asset_path.'/'.$path, '/'));
    }

    public function localThemeView(ThemeInfo $theme, $path = '')
    {
        $themes_path = rtrim($this->config('path'), '/\\');
        $path = rtrim($path, '/\\');

        return $themes_path . '/' . $theme->getSlug() . '/views/' . $path;
    }
}