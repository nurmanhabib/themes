<?php

namespace Nurmanhabib\Themes;

use InvalidArgumentException;
use Illuminate\Filesystem\Filesystem;
use Illuminate\View\FileViewFinder;

class ThemeFileViewFinder extends FileViewFinder
{
    public function addThemeViewLocation(ThemeInfo $theme)
    {
        $path1 = $theme->getViewPath();
        $path2 = reset($this->paths).'/'.$theme->getType();

        array_unshift($this->paths, $path1, $path2);
    }
}
